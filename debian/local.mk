############################ -*- Mode: Makefile -*- ###########################
## local.mk ---
## Author           : Manoj Srivastava ( srivasta@glaurung.green-gryphon.com )
## Created On       : Sat Nov 15 10:42:10 2003
## Created On Node  : glaurung.green-gryphon.com
## Last Modified By : Manoj Srivastava
## Last Modified On : Wed Oct  8 02:15:02 2008
## Last Machine Used: anzu.internal.golden-gryphon.com
## Update Count     : 20
## Status           : Unknown, Use with caution!
## HISTORY          :
## Description      :
##
## arch-tag: b07b1015-30ba-4b46-915f-78c776a808f4
##
###############################################################################

debian/stamp/BUILD/kernel-package: debian/stamp/build/kernel-package
debian/stamp/INST/kernel-package:  debian/stamp/install/kernel-package
debian/stamp/BIN/kernel-package:   debian/stamp/binary/kernel-package

testdir:
	$(checkdir)

debian/stamp/build/kernel-package:
	$(REASON)
	$(checkdir)
	@test -d debian/stamp/build || mkdir -p debian/stamp/build
	$(MAKE) genpo4a
	$(MAKE) build
	@echo done > $@

debian/stamp/install/kernel-package:
	$(REASON)
	$(checkdir)
	$(TESTROOT)
	@test -d debian/stamp/install || mkdir -p debian/stamp/install
	rm -rf $(TMPTOP)  $(TMPTOP).deb
	$(make_directory)  $(TMPTOP)/etc
	$(make_directory)  $(TMPTOP)/usr/bin
	$(make_directory)  $(TMPTOP)/usr/sbin
	$(make_directory)  $(TMPTOP)/usr/share/$(package)
	$(make_directory)  $(LINTIANDIR)
	echo "$(package): description-synopsis-might-not-be-phrased-properly" \
                                >>          $(LINTIANDIR)/$(package)
	chmod 0644                         $(LINTIANDIR)/$(package)
	$(MAKE) version=$(DEB_VERSION)  prefix=$(TMPTOP) install
	@echo done > $@

debian/stamp/binary/kernel-package: testroot
	$(REASON)
	$(checkdir)
	$(TESTROOT)
	@test -d debian/stamp/binary || mkdir -p debian/stamp/binary
	$(make_directory)  $(TMPTOP)/DEBIAN
	$(install_file)    debian/conffiles             $(TMPTOP)/DEBIAN/conffiles
	dpkg-gencontrol -isp
	$(create_md5sum)   $(TMPTOP)
	chown -R root:root $(TMPTOP)
	chmod -R u+w,go=rX $(TMPTOP)
	dpkg --build       $(TMPTOP) ..
	touch              stamp-binary
	@echo done > $@
